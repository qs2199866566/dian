import Vue from 'vue';

export function post(url,data={},callback,errcallback){
    Vue.http.headers.common['token']=window.localStorage.getItem("token");
    Vue.http.post(process.env.API_HOST+""+url,data,{emulateJSON:true}).then(res=>{
        if(res.body.error===0){
            callback(res.body);
        }else if(res.body.error===-1){
            this.$router.push("/login")
            this.showToast(res.body.message);
        }else{
            if(errcallback){
                errcallback(res.body);
            }else{
                this.showToast(res.body.message);
            }
            
        }
    },err=>{
        this.showToast("请刷新，重新连接")
    })
}