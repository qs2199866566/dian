// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// import axios from 'axios'
// import VueAxios from 'vue-axios'
// import Vant from 'vant';
import 'vant/lib/index.css';
// import install from '@/components/commons/index';
// import store from './store/index';
import utils from './util/index'
import { Toast, Dialog } from 'vant';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// import Qs from 'qs';
import VueResource from 'vue-resource';
import { post } from './util/resourse';



//定义全局变量
Vue.prototype.$post = post;

Vue.use(VueResource)
// Vue.use(Vant);
// Vue.use(install);
Vue.use(utils.utils);
// Vue.use(VueAxios, axios);
Vue.use(Toast);
Vue.use(Dialog);
Vue.use(ElementUI);

Vue.config.productionTip = false;

// 环境区分不同的baseURL
// if (process.env.API_HOST == '/api/') {
//   axios.defaults.baseURL = '/api'
// } else {
//   axios.defaults.baseURL = 'http://62.234.129.16/xyct/public/api.php/';
// }
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';

if (window.localStorage.getItem("token")) {
  Vue.http.headers.common['token'] = window.localStorage.getItem("token");
  // axios.defaults.headers.common['Authorization'] = window.localStorage.getItem("token");
  // axios.defaults.paramsSerializer = (params) => {
  //   return Qs.stringify(params, {arrayFormat: 'brackets'});
  // }
  // axios.defaults.paramsSerializer = (params) => {
  //   return Qs.stringify(params, {arrayFormat: 'brackets'});
  // }
}


// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
// }
// if(process.env.API_HOST=='/api/'){
//   axios.defaults.baseURL = 'http://192.168.3.198:8080/huzhu/api/'
// }else{
//   axios.defaults.baseURL = '/huzhu/api/'
// }

// axios.defaults.baseURL = '/api'

// var store1={
//   debug:true,
//   state:{
//     message:"hello"
//   }
// }



/* eslint-disable no-new */
const vue = new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  // data:{
  //   privateState: {},
  //   sharedState: store1.state
  // }
})

// 
var eventBus = {
  install(Vue, options) {
    Vue.prototype.$bus = vue
  }
}

Vue.use(eventBus);
