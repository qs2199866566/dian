import Vue from 'vue'
import Router from 'vue-router'
import routers from './routerList'
// import axios from 'axios'
// import VueAxios from 'vue-axios'

Vue.use(Router)


const router=new Router({
  routes:routers
})

router.beforeEach((to,from,next)=>{
  if(to.name=="zhuanpan"){
    document.documentElement.style.fontSize = ""
  }else{
    document.documentElement.style.fontSize = document.documentElement.clientWidth / 750*100 + 'px'
  }
if(to.name!="login"){
  // Vue.http.headers.common['token']=window.localStorage.getItem("token");
  // axios.defaults.headers.common['Authorization'] = window.localStorage.getItem("token");
}
  next();
})
router.afterEach((to, from) => {
  document.title = to.meta.title;
  window.onscroll="";
})

export default router;
