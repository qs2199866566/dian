const list = [
    {
        path: '/',
        redirect: "/login"
    }, {
        path: '/login',
        name: 'login',
        meta: {
            title: "登陆"
        },
        component: () => import('@/components/login/login.vue')
    }, {
        path: "/d_index",
        name: "d_index",
        component: () => import("@/components/d_index.vue"),
        children: [{
            path: '/index',
            name: 'index',
            component: () => import("@/components/index.vue"),
            meta: {
                title: "首页"
            }
        }, {
            path: "/role",
            name: "role",
            meta: {
                title: "角色"
            },
            component: () => import('@/components/role.vue')
        }, {
            path: "/user",
            name: "user",
            meta: {
                title: "部门和用户设置"
            },
            component: () => import('@/components/user.vue')
        }, {
            path: "/account",
            name: "account",
            meta: {
                title: "账号管理"
            },
            component: () => import('@/components/account.vue')
        }, {
            path: "/detail",
            name: "detail",
            meta: {
                title: "详情"
            },
            component: () => import('@/components/detail.vue')
        }]
    }
]

export default list;